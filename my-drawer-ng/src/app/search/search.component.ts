import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, View, ImageSource } from "@nativescript/core";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { NoticiasService } from "../domain/noticias.service";
import * as Toast from "nativescript-toasts";
import * as SocialShare from "@nativescript/social-share";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html" /*,
    providers: [NoticiasService]*/
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
    fav: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    constructor(public noticias: NoticiasService,
        private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if(f != null) {
                    Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
                }
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onPull(e) { 
        console.log(e); 
        const pullRefresh = e.object; 
        setTimeout(() => { 
            this.noticias.agregar("xxxxxxx"); 
            pullRefresh.refreshing = false; 
        }, 2000); 
    }

    onLongPress(s):void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso");
    }

    onDoubleTap() {
        console.log("onDoubleTap");
        ImageSource.fromUrl("https://cdn.pixabay.com/photo/2012/04/26/19/47/penguin-42936_960_720.png").then((image) => {
            SocialShare.shareImage(image, "Asunto: imagen compartida desde el curso");
        });
    }

    buscarAhora(s: string) {
        console.log("buscarAhora");
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora: " + e);
            Toast.show({ text: "Error en la búsqueda", duration: Toast.DURATION.SHORT });
        });
        // this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);
        // const layout = <View>this.layout.nativeElement;
        // layout.animate({
        //     backgroundColor: new Color("blue"),
        //     duration: 300,
        //     delay: 150
        // }).then(() => layout.animate({
        //     backgroundColor: new Color("white"),
        //     duration: 300,
        //     delay: 150
        // }));
    }

    // onTap(event, x: Noticia) {

    //     console.dir("tabfav" + event);
    //     console.log(x.favorita);
    //     if (x.favorita === false) {
    //         x.favorita = true;
    //         this.noticias.postFavorita(x);
            
    //     } else {
    //         x.favorita = false;
    //         this.noticias.deleteFavorita(x);
            
    //     }
    //     console.log(x.favorita);
    // }
}
