import { NativeScriptConfig } from '@nativescript/core'

export default {
  id: 'org.nativescript.mydrawerng',
  appResourcesPath: 'App_Resources',
  android: {
    discardUncaughtJsExceptions: true,
    v8Flags: '--expose_gc',
    markingMode: 'none',
    suppressCallJSMethodExceptions: false
  },
  appPath: 'src',
} as NativeScriptConfig
